package com.leslie.powercontrol.appwidget;

import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Constants;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.PowerManager;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Log;
import android.widget.RemoteViews;

public class BrightnessStateTracker extends StateTracker {
	
    /** Minimum brightness at which the indicator is shown at half-full and ON */
    private static final float HALF_BRIGHTNESS_THRESHOLD = 0.3f;
    /** Minimum brightness at which the indicator is shown at full */
    private static final float FULL_BRIGHTNESS_THRESHOLD = 0.8f;
    
	public BrightnessStateTracker() {
		super();
		PowerControlAppWidgetProvider.registerObservedContent(Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS), this);
		PowerControlAppWidgetProvider.registerObservedContent(Settings.System.getUriFor(Settings.System.SCREEN_BRIGHTNESS_MODE), this);
	}

	@Override
	public void setImageViewResources(Context context, RemoteViews views) {
        int containerId = getButtonId();
        int buttonId = getButtonId();
        
		if (getBrightnessMode(context)) {
            views.setContentDescription(containerId,
                    context.getString(R.string.gadget_brightness_template,
                            context.getString(R.string.gadget_brightness_state_auto)));
            views.setImageViewResource(buttonId,
                    R.drawable.icon_toggle_brightness_auto);
            views.setInt(buttonId, "setColorFilter", context.getResources().getColor(R.color.btn_default_color_enabled));
		} else {
            final int brightness = getBrightness(context);
            final PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
            // Set the icon
            final int full = (int)(PowerManagerHelper.getMaximumScreenBrightnessSetting(pm)
                    * FULL_BRIGHTNESS_THRESHOLD);
            final int half = (int)(PowerManagerHelper.getMaximumScreenBrightnessSetting(pm)
                    * HALF_BRIGHTNESS_THRESHOLD);
            if (brightness > full) {
                views.setContentDescription(containerId,
                        context.getString(R.string.gadget_brightness_template,
                                context.getString(R.string.gadget_brightness_state_full)));
                views.setImageViewResource(buttonId,
                        R.drawable.icon_toggle_brightness_full);
                views.setInt(buttonId, "setColorFilter", context.getResources().getColor(R.color.btn_default_color_enabled));
            } else if (brightness > half) {
                views.setContentDescription(containerId,
                        context.getString(R.string.gadget_brightness_template,
                                context.getString(R.string.gadget_brightness_state_half)));
                views.setImageViewResource(buttonId,
                        R.drawable.icon_toggle_brightness_half);
                views.setInt(buttonId, "setColorFilter", context.getResources().getColor(R.color.btn_default_color_enabled));
            } else {
                views.setContentDescription(containerId,
                        context.getString(R.string.gadget_brightness_template,
                                context.getString(R.string.gadget_brightness_state_off)));
                views.setImageViewResource(buttonId,
                        R.drawable.icon_toggle_brightness_off);
                views.setInt(buttonId, "setColorFilter", context.getResources().getColor(R.color.btn_default_color_disabled));
            }

        }
	}

	@Override
	public void onActualStateChange(Context context, Intent intent) {

	}

	@Override
	public void onClick(Context context) {
        try {
        	
            PowerManager pm = (PowerManager)context.getSystemService(Context.POWER_SERVICE);

            ContentResolver cr = context.getContentResolver();
            int brightness = Settings.System.getInt(cr,
                    Settings.System.SCREEN_BRIGHTNESS);
            int brightnessMode = Settings.System.getInt(cr,
                    Settings.System.SCREEN_BRIGHTNESS_MODE);

            // Rotate AUTO -> MINIMUM -> DEFAULT -> MAXIMUM
            // Technically, not a toggle...
            int minBrightness = PowerManagerHelper.getMinimumScreenBrightnessSetting(pm);
            int defBrightness = PowerManagerHelper.getDefaultScreenBrightnessSetting(pm);
            int maxBrightness = PowerManagerHelper.getMaximumScreenBrightnessSetting(pm);
            if (brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC) {
                brightness = minBrightness;
                brightnessMode = Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL;
            } else if (brightness < defBrightness) {
                brightness = defBrightness;
            } else if (brightness < maxBrightness) {
                brightness = maxBrightness;
            } else {
                brightnessMode = Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
                brightness = minBrightness;
            }

            Settings.System.putInt(context.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE,
                    brightnessMode);

            if (brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL) {
                PowerManagerHelper.setBacklightBrightness(pm, brightness);
                Settings.System.putInt(cr, Settings.System.SCREEN_BRIGHTNESS, brightness);
            }

        } catch (Settings.SettingNotFoundException e) {
            Log.d(getName(), "toggleBrightness: " + e);
        }
    }

    /**
     * Gets brightness level.
     *
     * @param context
     * @return brightness level between 0 and 255.
     */
    private static int getBrightness(Context context) {
        try {
            int brightness = Settings.System.getInt(context.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS);
            return brightness;
        } catch (Exception e) {
        }
        return 0;
    }
    
    /**
     * Gets state of brightness mode.
     *
     * @param context
     * @return true if auto brightness is on.
     */
    private static boolean getBrightnessMode(Context context) {
        try {
            int brightnessMode = Settings.System.getInt(context.getContentResolver(),
                    Settings.System.SCREEN_BRIGHTNESS_MODE);
            return brightnessMode == Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC;
        } catch (Exception e) {
        }
        return false;
    }

	@Override
	public String getName() {
		return Constants.WIDGET_BRIGHTNESS;
	}

	@Override
	public void onChangeUri(Context context, Uri uri) {
		PowerControlAppWidgetProvider.updateWidget(context);
	}
	
	
    
}
