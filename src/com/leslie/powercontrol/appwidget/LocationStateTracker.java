package com.leslie.powercontrol.appwidget;

import java.io.DataOutputStream;

import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.UserManager;
import android.provider.Settings;
import android.util.Log;


//import com.chrisplus.rootmanager.RootManager;
//import com.chrisplus.rootmanager.container.Result;
import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Constants;
import com.leslie.powercontrol.util.DebugLog;
import com.leslie.powercontrol.util.RootChecker;
import com.leslie.powercontrol.util.ShellUtils;
import com.leslie.powercontrol.util.ShellUtils.CommandResult;
import com.leslie.powercontrol.util.Utils;

public class LocationStateTracker extends ToggleStateTracker {
	
    private int mCurrentLocationMode = Settings.Secure.LOCATION_MODE_OFF;

    public int getButtonDescription() { return R.string.gadget_location; }
    public int getButtonImageId(boolean on) {
        return R.drawable.icon_toggle_location;
    }
    
    @Override
    public int getActualState(Context context) {
        ContentResolver resolver = context.getContentResolver();
        mCurrentLocationMode = Settings.Secure.getInt(resolver,
                Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);
        return (mCurrentLocationMode == Settings.Secure.LOCATION_MODE_OFF || mCurrentLocationMode == Settings.Secure.LOCATION_MODE_BATTERY_SAVING)
                ? STATE_DISABLED : STATE_ENABLED;
    }

    @Override
    public void onActualStateChange(Context context, Intent unused) {
        // Note: the broadcast location providers changed intent
        // doesn't include an extras bundles saying what the new value is.
        setCurrentState(context, getActualState(context));
    }

    @Override
    public void requestStateChange(final Context context, final boolean desiredState) {
        final ContentResolver resolver = context.getContentResolver();
        final int currentMode = Settings.Secure.getInt(resolver,
                Settings.Secure.LOCATION_MODE, Settings.Secure.LOCATION_MODE_OFF);
        
        if (Utils.hasWriteSecurePermissions(context)) {
	        new AsyncTask<Void, Void, Boolean>() {
	            @Override
	            protected Boolean doInBackground(Void... args) {
	                final UserManager um = 
	                        (UserManager) context.getSystemService(Context.USER_SERVICE);
	                if (!um.getUserRestrictions().getBoolean(UserManager.DISALLOW_SHARE_LOCATION, false)) {

	                    int mode = changeLocationMode(currentMode);
	                    Settings.Secure.putInt(resolver, Settings.Secure.LOCATION_MODE, mode);
	                    return mode != Settings.Secure.LOCATION_MODE_OFF;

	                }
	
	                return getActualState(context) == STATE_ENABLED;
	            }
	
	            @Override
	            protected void onPostExecute(Boolean result) {
	                setCurrentState(
	                    context,
	                    result ? STATE_ENABLED : STATE_DISABLED);
	                PowerControlAppWidgetProvider.updateWidget(context);
	            }
	        }.execute();
        } else if (RootChecker.getInstance().hasRooted()) {
            final int mode = changeLocationMode(currentMode);
            
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... args) {

                    StringBuilder command = new StringBuilder().append("LD_LIBRARY_PATH=/vendor/lib:/system/lib settings put secure ")
                    		.append(Settings.Secure.LOCATION_PROVIDERS_ALLOWED + " ");

                    switch (mode) {
                        case Settings.Secure.LOCATION_MODE_OFF:
                        	command.append("");
                            break;
                        case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
                        	command.append(LocationManager.GPS_PROVIDER);
                            break;
                        case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
                        	command.append(LocationManager.NETWORK_PROVIDER);
                            break;
                        case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
                        	command.append(LocationManager.GPS_PROVIDER + "," + LocationManager.NETWORK_PROVIDER);
                            break;
                        default:
                            throw new IllegalArgumentException("Invalid location mode: " + mode);
                    }
                    
//                    StringBuilder command = new StringBuilder().append("LD_LIBRARY_PATH=/vendor/lib:/system/lib settings get secure ")
//                    		.append(Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                    DebugLog.d("command : " + command.toString());
                    CommandResult commandResult = ShellUtils.execCommand(command.toString(), !Utils.isSystemApplication(context), true);
                    if (commandResult.successMsg != null
                            && (commandResult.successMsg.contains("Success") || commandResult.successMsg.contains("success"))) {
                        return true;
                    }

                    DebugLog.e(
                            new StringBuilder().append("wreite settings successMsg:").append(commandResult.successMsg)
                                    .append(", ErrorMsg:").append(commandResult.errorMsg).toString());

                    if (commandResult.result != 0) {
                    	startLocationSettingsActivity(context);
                    }
                	return false;
                }
                
                @Override
                protected void onPostExecute(Boolean result) {
                	setCurrentState(context, getActualState(context));
                    PowerControlAppWidgetProvider.updateWidget(context);
                }
            }.execute();
        } else {
        	
	        new AsyncTask<Void, Void, Boolean>() {
	            @Override
	            protected Boolean doInBackground(Void... args) {
	            	startLocationSettingsActivity(context);
	
	                return getActualState(context) == STATE_ENABLED;
	            }
	
	            @Override
	            protected void onPostExecute(Boolean result) {
	                setCurrentState(
	                    context,
	                    result ? STATE_ENABLED : STATE_DISABLED);
	                PowerControlAppWidgetProvider.updateWidget(context);
	            }
	        }.execute();
        }
    }
    
	private void startLocationSettingsActivity(Context context) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
	}
	
    private int changeLocationMode(int currentMode){

        int mode = Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
        switch (currentMode) {
            case Settings.Secure.LOCATION_MODE_HIGH_ACCURACY:
                mode = Settings.Secure.LOCATION_MODE_BATTERY_SAVING;
                break;
            case Settings.Secure.LOCATION_MODE_BATTERY_SAVING:
                mode = Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
                break;
            case Settings.Secure.LOCATION_MODE_SENSORS_ONLY:
                mode = Settings.Secure.LOCATION_MODE_OFF;
                break;
            case Settings.Secure.LOCATION_MODE_OFF:
                mode = Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;
                break;       
        }
        
        return mode;
    }
    
    public String getName() {
    	return Constants.WIDGET_LOCATION;
    }
    
    public String getReceiverAction() {
    	return LocationManager.MODE_CHANGED_ACTION;
    }
    
}
