package com.leslie.powercontrol.appwidget;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.RemoteViews;

import com.leslie.powercontrol.R;

import com.leslie.powercontrol.util.Constants;

/**
 * The state machine for a setting's toggling, tracking reality
 * versus the user's intent.
 *
 * This is necessary because reality moves relatively slowly
 * (turning on &amp; off radio drivers), compared to user's
 * expectations.
 */
public abstract  class ToggleStateTracker extends StateTracker{
    //static final String TAG = "ToggleStateTracker";

    // This widget keeps track of two sets of states:
    // "3-state": STATE_DISABLED, STATE_ENABLED, STATE_INTERMEDIATE
    // "5-state": STATE_DISABLED, STATE_ENABLED, STATE_TURNING_ON, STATE_TURNING_OFF, STATE_UNKNOWN
    public static final int STATE_DISABLED = 0;
    public static final int STATE_ENABLED = 1;
    public static final int STATE_TURNING_ON = 2;
    public static final int STATE_TURNING_OFF = 3;
    public static final int STATE_UNKNOWN = 4;
    public static final int STATE_INTERMEDIATE = 5;
    
    // Is the state in the process of changing?
    private boolean mInTransition = false;
    private Boolean mActualState = null;  // initially not set
    private Boolean mIntendedState = null;  // initially not set

    // Did a toggle request arrive while a state update was
    // already in-flight?  If so, the mIntendedState needs to be
    // requested when the other one is done, unless we happened to
    // arrive at that state already.
    private boolean mDeferredStateChangeRequestNeeded = false;

    /**
     * User pressed a button to change the state.  Something
     * should immediately appear to the user afterwards, even if
     * we effectively do nothing.  Their press must be heard.
     */
    private final void toggleState(Context context) {
        int currentState = getTriState(context);
        boolean newState = false;
        switch (currentState) {
            case STATE_ENABLED:
                newState = false;
                break;
            case STATE_DISABLED:
                newState = true;
                break;
            case STATE_INTERMEDIATE:
                if (mIntendedState != null) {
                    newState = !mIntendedState;
                }
                break;
        }
        mIntendedState = newState;
        if (mInTransition) {
            // We don't send off a transition request if we're
            // already transitioning.  Makes our state tracking
            // easier, and is probably nicer on lower levels.
            // (even though they should be able to take it...)
            mDeferredStateChangeRequestNeeded = true;
        } else {
            mInTransition = true;
            requestStateChange(context, newState);
        }
    }
    
    /**
     * Returns the resource ID of the setting's content description.
     */
    public abstract int getButtonDescription();

    /**
     * Returns the resource ID of the image to show as a function of
     * the on-vs-off state.
     */
    public abstract int getButtonImageId(boolean on);
    
    /**
     * Updates the remote views depending on the state (off, on,
     * turning off, turning on) of the setting.
     */
    public final void setImageViewResources(Context context, RemoteViews views) {
        int containerId = getButtonId();
        int buttonId = getButtonId();
        switch (getTriState(context)) {
            case STATE_DISABLED:
            	Log.d(getName(), "[setImageViewResources] STATE_DISABLED");
                views.setContentDescription(containerId,
                    getContentDescription(context, R.string.gadget_state_off));
                views.setImageViewResource(buttonId, getButtonImageId(false));
                views.setInt(buttonId, "setColorFilter", context.getResources().getColor(R.color.btn_default_color_disabled));
                views.setInt(buttonId, "setImageAlpha", 0x66);
                break;
            case STATE_ENABLED:
            	Log.d(getName(), "[setImageViewResources] STATE_ENABLED");
                views.setContentDescription(containerId,
                    getContentDescription(context, R.string.gadget_state_on));
                views.setImageViewResource(buttonId, getButtonImageId(true));
                views.setInt(buttonId, "setColorFilter", context.getResources().getColor(R.color.btn_default_color_enabled));
                views.setInt(buttonId, "setImageAlpha", 0xff);
                break;
            case STATE_INTERMEDIATE:
                // In the transitional state, the bottom green bar
                // shows the tri-state (on, off, transitioning), but
                // the top dark-gray-or-bright-white logo shows the
                // user's intent.  This is much easier to see in
                // sunlight.
            	Log.d(getName(), "[setImageViewResources] STATE_INTERMEDIATE");
                if (isTurningOn()) {
                    views.setContentDescription(containerId,
                        getContentDescription(context, R.string.gadget_state_turning_on));
                    views.setImageViewResource(buttonId, getButtonImageId(true));

                } else {
                    views.setContentDescription(containerId,
                        getContentDescription(context, R.string.gadget_state_turning_off));
                    views.setImageViewResource(buttonId, getButtonImageId(false));
                }
                views.setInt(buttonId, "setColorFilter", context.getResources().getColor(R.color.btn_default_color_intermediat));
                views.setInt(buttonId, "setImageAlpha", 0xff);
                break;
        }
    }

    /**
     * Returns the gadget state template populated with the gadget
     * description and state.
     */
    private final String getContentDescription(Context context, int stateResId) {
        final String gadget = context.getString(getButtonDescription());
        final String state = context.getString(stateResId);
        return context.getString(R.string.gadget_state_template, gadget, state);
    }
    
    /**
     * Update internal state from a broadcast state change.
     */
    public abstract void onActualStateChange(Context context, Intent intent);

    /**
     * Sets the value that we're now in.  To be called from onActualStateChange.
     *
     * @param newState one of STATE_DISABLED, STATE_ENABLED, STATE_TURNING_ON,
     *                 STATE_TURNING_OFF, STATE_UNKNOWN
     */
    protected final void setCurrentState(Context context, int newState) {
        final boolean wasInTransition = mInTransition;
        switch (newState) {
            case STATE_DISABLED:
                mInTransition = false;
                mActualState = false;
                break;
            case STATE_ENABLED:
                mInTransition = false;
                mActualState = true;
                break;
            case STATE_TURNING_ON:
                mInTransition = true;
                mActualState = false;
                break;
            case STATE_TURNING_OFF:
                mInTransition = true;
                mActualState = true;
                break;
        }

        if (wasInTransition && !mInTransition) {
            if (mDeferredStateChangeRequestNeeded) {
                Log.v(getName(), "processing deferred state change");
                if (mActualState != null && mIntendedState != null &&
                    mIntendedState.equals(mActualState)) {
                    Log.v(getName(), "... but intended state matches, so no changes.");
                } else if (mIntendedState != null) {
                    mInTransition = true;
                    requestStateChange(context, mIntendedState);
                }
                mDeferredStateChangeRequestNeeded = false;
            }
        }
    }


    /**
     * If we're in a transition mode, this returns true if we're
     * transitioning towards being enabled.
     */
    public final boolean isTurningOn() {
        return mIntendedState != null && mIntendedState;
    }

    /**
     * Returns simplified 3-state value from underlying 5-state.
     *
     * @param context
     * @return STATE_ENABLED, STATE_DISABLED, or STATE_INTERMEDIATE
     */
    public final int getTriState(Context context) {
        if (mInTransition) {
            // If we know we just got a toggle request recently
            // (which set mInTransition), don't even ask the
            // underlying interface for its state.  We know we're
            // changing.  This avoids blocking the UI thread
            // during UI refresh post-toggle if the underlying
            // service state accessor has coarse locking on its
            // state (to be fixed separately).
            return STATE_INTERMEDIATE;
        }
        switch (getActualState(context)) {
            case STATE_DISABLED:
                return STATE_DISABLED;
            case STATE_ENABLED:
                return STATE_ENABLED;
            default:
                return STATE_INTERMEDIATE;
        }
    }

    /**
     * Gets underlying actual state.
     *
     * @param context
     * @return STATE_ENABLED, STATE_DISABLED, STATE_ENABLING, STATE_DISABLING,
     *         or or STATE_UNKNOWN.
     */
    public abstract int getActualState(Context context);

    /**
     * Actually make the desired change to the underlying radio
     * API.
     */
    protected abstract void requestStateChange(Context context, boolean desiredState);
    
    
    @Override
	public void onClick(Context context) {
		toggleState(context);
	}
    
}
