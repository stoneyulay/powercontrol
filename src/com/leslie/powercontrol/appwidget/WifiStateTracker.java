package com.leslie.powercontrol.appwidget;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Constants;

public class WifiStateTracker extends ToggleStateTracker {
    public int getButtonDescription() { return R.string.gadget_wifi; }
    public int getButtonImageId(boolean on) {
        return R.drawable.icon_toggle_wifi;
    }
    
    @Override
    public int getActualState(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager != null) {
            return wifiStateToFiveState(wifiManager.getWifiState());
        }
        return STATE_UNKNOWN;
    }

    @Override
    protected void requestStateChange(Context context, final boolean desiredState) {
        final WifiManager wifiManager =
                (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager == null) {
            Log.d(getName(), "No wifiManager.");
            return;
        }

        // Actually request the wifi change and persistent
        // settings write off the UI thread, as it can take a
        // user-noticeable amount of time, especially if there's
        // disk contention.
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... args) {
                wifiManager.setWifiEnabled(desiredState);
                return null;
            }
        }.execute();
    }

    @Override
    public void onActualStateChange(Context context, Intent intent) {
        if (!WifiManager.WIFI_STATE_CHANGED_ACTION.equals(intent.getAction())) {
            return;
        }
        int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
        setCurrentState(context, wifiStateToFiveState(wifiState));
    }

    /**
     * Converts WifiManager's state values into our
     * Wifi/Bluetooth-common state values.
     */
    private static int wifiStateToFiveState(int wifiState) {
        switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLED:
                return STATE_DISABLED;
            case WifiManager.WIFI_STATE_ENABLED:
                return STATE_ENABLED;
            case WifiManager.WIFI_STATE_DISABLING:
                return STATE_TURNING_OFF;
            case WifiManager.WIFI_STATE_ENABLING:
                return STATE_TURNING_ON;
            default:
                return STATE_UNKNOWN;
        }
    }
    
    public String getName() {
    	return Constants.WIDGET_WIFI;
    }
    
    public String getReceiverAction() {
    	return WifiManager.WIFI_STATE_CHANGED_ACTION;
    }
}
