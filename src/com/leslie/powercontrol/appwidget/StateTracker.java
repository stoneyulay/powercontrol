package com.leslie.powercontrol.appwidget;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.RemoteViews;

public abstract class StateTracker {
	public abstract void setImageViewResources(Context context, RemoteViews views);
	
    public abstract void onActualStateChange(Context context, Intent intent);
    
    public abstract void onClick(Context context);
    
    public abstract String getName();
    
    public String getReceiverAction() {
    	return null;
    }
    
    private int mButtonId;
    
    public int getButtonId() {
    	return mButtonId;
    }
    
    public void setButtonId(int id) {
    	mButtonId = id;
    }
    
    public void onChangeUri(Context context, Uri uri) {}

}