package com.leslie.powercontrol.appwidget;

import java.lang.reflect.Method;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;

import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Constants;

public class MobileDataStateTracker extends ToggleStateTracker {
	
    private static final String MOBILE_DATA = "mobile_data";

	public MobileDataStateTracker() {
		super();
		PowerControlAppWidgetProvider.registerObservedContent(Settings.Global.getUriFor(MOBILE_DATA), this);
	}
	
	public int getButtonDescription() {	return R.string.gadget_mobile_data;	}
	public int getButtonImageId(boolean on) {
		return R.drawable.icon_toggle_mobile_data;
	}

	@Override
	public int getActualState(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Boolean enabled = getMobileDataEnabled(cm);
        if (enabled != null) {
        	return enabled.booleanValue() ? STATE_ENABLED : STATE_DISABLED;
        }
        return STATE_UNKNOWN;
	}

	@Override
	public void onActualStateChange(Context context, Intent intent) {
		setCurrentState(context, getActualState(context));
	}
	
	@Override
    protected void requestStateChange(final Context context, final boolean desiredState) {
		final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		final boolean currentState = getMobileDataEnabled(cm).booleanValue();
		
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... args) {
                // Turning mobile data on.
            	setMobileDataEnabled(cm, desiredState);
            	return desiredState;
            }
            
            @Override
            protected void onPostExecute(Boolean result) {
                setCurrentState(
                    context,
                    result ? STATE_ENABLED : STATE_DISABLED);
                PowerControlAppWidgetProvider.updateWidget(context);
            }
        }.execute();
    }

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return Constants.WIDGET_MOBILEDATA;
	}
    
	
	@Override
	public void onChangeUri(Context context, Uri uri) {
		Log.d(getName(), "onChangeUri");
		setCurrentState(context, getActualState(context));
		PowerControlAppWidgetProvider.updateWidget(context);
	}

	/**
	 * @return null if unconfirmed
	 */
	private Boolean getMobileDataEnabled(ConnectivityManager cm){

	    try {
	        Class<?> c = Class.forName(cm.getClass().getName());
	        Method m = c.getDeclaredMethod("getMobileDataEnabled");
	        m.setAccessible(true);
	        return (Boolean)m.invoke(cm);
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	}

	/**
	 * @return null if unconfirmed
	 */
	private Boolean setMobileDataEnabled(ConnectivityManager cm, boolean enabled){
		
	    try {
	    	Log.d(getName(),"setMobileDataEnabled enabled = " + enabled);
	        Class<?> c = Class.forName(cm.getClass().getName());
	        Method m = c.getDeclaredMethod("setMobileDataEnabled", boolean.class);
	        m.setAccessible(true);
	        return (Boolean)m.invoke(cm, enabled);
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	}
}
