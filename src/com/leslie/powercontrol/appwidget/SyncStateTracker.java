package com.leslie.powercontrol.appwidget;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;

import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Constants;

public class SyncStateTracker extends ToggleStateTracker {

    public int getButtonDescription() { return R.string.gadget_sync; }
    public int getButtonImageId(boolean on) {
        return R.drawable.icon_toggle_sync;
    }
    
    @Override
    public int getActualState(Context context) {
        boolean on = ContentResolver.getMasterSyncAutomatically();
        return on ? STATE_ENABLED : STATE_DISABLED;
    }

    @Override
    public void onActualStateChange(Context context, Intent unused) {
        setCurrentState(context, getActualState(context));
    }

    @Override
    public void requestStateChange(final Context context, final boolean desiredState) {
        final ConnectivityManager connManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final boolean sync = ContentResolver.getMasterSyncAutomatically();

        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... args) {
                // Turning sync on.
                if (desiredState) {
                    if (!sync) {
                        ContentResolver.setMasterSyncAutomatically(true);
                    }
                    return true;
                }

                // Turning sync off
                if (sync) {
                    ContentResolver.setMasterSyncAutomatically(false);
                }
                return false;
            }

            @Override
            protected void onPostExecute(Boolean result) {
                setCurrentState(
                    context,
                    result ? STATE_ENABLED : STATE_DISABLED);
                PowerControlAppWidgetProvider.updateWidget(context);
            }
        }.execute();
    }
    
    public String getName() {
    	return Constants.WIDGET_SYNC;
    }
    
    public String getReceiverAction() {
    	return "com.android.sync.SYNC_CONN_STATUS_CHANGED";
    }
}
