package com.leslie.powercontrol.appwidget;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Constants;

public class BluetoothStateTracker extends ToggleStateTracker {
    public int getButtonDescription() { return R.string.gadget_bluetooth; }
    public int getButtonImageId(boolean on) {
        return R.drawable.icon_toggle_bluetooth;
    }
    
    @Override
    public int getActualState(Context context) {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return STATE_UNKNOWN; // On emulator?
        }
        return bluetoothStateToFiveState(mBluetoothAdapter
                .getState());
    }

    @Override
    protected void requestStateChange(Context context,
            final boolean desiredState) {
        // Actually request the Bluetooth change and persistent
        // settings write off the UI thread, as it can take a
        // user-noticeable amount of time, especially if there's
        // disk contention.
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... args) {
                BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                if(mBluetoothAdapter.isEnabled()) {
                    mBluetoothAdapter.disable();
                } else {
                    mBluetoothAdapter.enable();
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void onActualStateChange(Context context, Intent intent) {
        if (!BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
            return;
        }
        int bluetoothState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
        setCurrentState(context, bluetoothStateToFiveState(bluetoothState));
    }

    /**
     * Converts BluetoothAdapter's state values into our
     * Wifi/Bluetooth-common state values.
     */
    private static int bluetoothStateToFiveState(int bluetoothState) {
        switch (bluetoothState) {
            case BluetoothAdapter.STATE_OFF:
                return STATE_DISABLED;
            case BluetoothAdapter.STATE_ON:
                return STATE_ENABLED;
            case BluetoothAdapter.STATE_TURNING_ON:
                return STATE_TURNING_ON;
            case BluetoothAdapter.STATE_TURNING_OFF:
                return STATE_TURNING_OFF;
            default:
                return STATE_UNKNOWN;
        }
    }
    
    public String getName() {
    	return Constants.WIDGET_BLUETOOTH;
    }
    
    public String getReceiverAction() {
    	return BluetoothAdapter.ACTION_STATE_CHANGED;
    }
}
