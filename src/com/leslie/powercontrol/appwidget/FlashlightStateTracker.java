package com.leslie.powercontrol.appwidget;


import com.leslie.powercontrol.R;
import com.leslie.powercontrol.flashlight.TorchSwitch;
import com.leslie.powercontrol.util.Constants;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;

public class FlashlightStateTracker extends ToggleStateTracker {

	public int getButtonDescription() { return R.string.gadget_flashlight; }
	public int getButtonImageId(boolean on) {
		return R.drawable.icon_toggle_flash;
	}
	
    private boolean mFlashOn;

	@Override
	public int getActualState(Context context) {
		return mFlashOn ? STATE_ENABLED : STATE_DISABLED;
	}
	
	@Override
	public void onActualStateChange(Context context, Intent intent) {
		mFlashOn = intent.getIntExtra("state", 0) != 0;
		setCurrentState(context, getActualState(context));
	}

	@Override
	protected void requestStateChange(Context context, boolean desiredState) {
		// TODO Auto-generated method stub
        Intent intent = new Intent(TorchSwitch.TOGGLE_FLASHLIGHT);
        context.sendBroadcast(intent);
	}

	@Override
	public String getName() {
		return Constants.WIDGET_FLASHLIGHT;
	}
	
    public String getReceiverAction() {
    	return TorchSwitch.TORCH_STATE_CHANGED;
    }

}
