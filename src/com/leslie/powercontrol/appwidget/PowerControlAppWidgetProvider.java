package com.leslie.powercontrol.appwidget;

import java.util.ArrayList;
import java.util.HashMap;

import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Utils;
import com.leslie.powercontrol.util.Constants;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.UserManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;

public class PowerControlAppWidgetProvider extends AppWidgetProvider {
	private static final String TAG = "PowerControlAppWidgetProvider";
	
    static final ComponentName THIS_APPWIDGET =
            new ComponentName("com.leslie.powercontrol",
                    "com.leslie.powercontrol.appwidget.PowerControlAppWidgetProvider");

    private static final int[] REMOTE_BUTTONS_ID = {
        R.id.btn_1,
        R.id.btn_2,
        R.id.btn_3,
        R.id.btn_4,
        R.id.btn_5,
        R.id.btn_6,
        R.id.btn_7,
        R.id.btn_8
    };
    
    private static final int REMOTE_BUTTONS_NUM_MAX = REMOTE_BUTTONS_ID.length;
    private static ArrayList<StateTracker> sStateTrackers = new ArrayList<StateTracker>();
    
	@Override
	public void onAppWidgetOptionsChanged(Context context,
			AppWidgetManager appWidgetManager, int appWidgetId,
			Bundle newOptions) {
		Log.d(TAG, "onAppWidgetOptionsChanged");
		super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId,
				newOptions);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		Log.d(TAG, "onDeleted");
		super.onDeleted(context, appWidgetIds);
	}

	@Override
	public void onDisabled(Context context) {
		Log.d(TAG, "onDisabled");
		super.onDisabled(context);
	}

	@Override
	public void onEnabled(Context context) {
		Log.d(TAG, "onEnabled");
		super.onEnabled(context);
	}

	@Override
	public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive action: " + intent.getAction());
		super.onReceive(context, intent);
		boolean handled = false;
        String action = intent.getAction();
		if (intent.hasCategory(Intent.CATEGORY_ALTERNATIVE)) {
            Uri data = intent.getData();
            String buttonId = data.getSchemeSpecificPart();
	    	for (StateTracker st : sStateTrackers) {
	    		if (st.getName().equals(buttonId)) {
	    			st.onClick(context);
	    			break;
	    		}
	    	}
	    	
	    	handled = true;
		} else {
	    	for (StateTracker st : sStateTrackers) {
	    		String stAction = st.getReceiverAction();
	    		if (stAction != null && stAction.equals(action)) {
	                st.onActualStateChange(context, intent);
	                handled = true;
	                break;
	    		}
	    	}
		}
		
        // State changes fall through
		if (handled) {
			updateWidget(context);
		}
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
        Log.d(TAG, "onUpdate");
        // Update each requested appWidgetId
        RemoteViews view = buildUpdate(context);

        for (int i = 0; i < appWidgetIds.length; i++) {
            appWidgetManager.updateAppWidget(appWidgetIds[i], view);
        }
	}

    /**
     * Load image for given widget and build {@link RemoteViews} for it.
     */
    static RemoteViews buildUpdate(Context context) {
        RemoteViews views = new RemoteViews(context.getPackageName(),
                R.layout.appwidget_default_60);
        if (sStateTrackers.size() == 0) {
        	setupButtons(context);
        }
        for (int i = 0; i < REMOTE_BUTTONS_ID.length; i++) {
        	if (i < sStateTrackers.size()) {
            	StateTracker st = sStateTrackers.get(i);
            	st.setButtonId(REMOTE_BUTTONS_ID[i]);
        	} else {
        		views.setViewVisibility(REMOTE_BUTTONS_ID[i], View.GONE);
        	}

        }
        
    	for (StateTracker st : sStateTrackers) {
            views.setOnClickPendingIntent(st.getButtonId(), getLaunchPendingIntent(context,
                    st.getName()));
    	}
    	
        updateButtons(views, context);
        return views;
    }

    /**
     * Updates the widget when something changes, or when a button is pushed.
     *
     * @param context
     */
    public static void updateWidget(Context context) {
        RemoteViews views = buildUpdate(context);
        // Update specific list of appWidgetIds if given, otherwise default to all
        final AppWidgetManager gm = AppWidgetManager.getInstance(context);
        gm.updateAppWidget(THIS_APPWIDGET, views);
    }

    /**
     * Updates the buttons based on the underlying states of wifi, etc.
     *
     * @param views   The RemoteViews to update.
     * @param context
     */
    private static void updateButtons(RemoteViews views, Context context) {
    	for (StateTracker st : sStateTrackers) {
    		st.setImageViewResources(context, views);
    	}
    }
    
    /**
     * Creates PendingIntent to notify the widget of a button click.
     *
     * @param context
     * @return
     */
    private static PendingIntent getLaunchPendingIntent(Context context,
            String buttonId) {
        Intent launchIntent = new Intent();
        launchIntent.setClass(context, PowerControlAppWidgetProvider.class);
        launchIntent.addCategory(Intent.CATEGORY_ALTERNATIVE);
        launchIntent.setData(Uri.parse("custom:" + buttonId));
        PendingIntent pi = PendingIntent.getBroadcast(context, 0 /* no requestCode */,
                launchIntent, 0 /* no flags */);
        return pi;
    }

    private static final Handler mHandler = new Handler();
    private static ContentObserver sObserver;
    private static HashMap<Uri, ArrayList<StateTracker>> sObserverMap = new HashMap<Uri, ArrayList<StateTracker>>();
    
	private static void loadTrackers(Context context) {
    	
        // Filter items not compatible with device
        boolean cameraSupported = Utils.deviceSupportsCamera();
        boolean bluetoothSupported = Utils.deviceSupportsBluetooth();
        boolean gpsSupported = Utils.deviceSupportsGps(context);

        if (!bluetoothSupported) {
        	Constants.WIDGETS_DEFAULT.remove(Constants.WIDGET_BLUETOOTH);
        }

        if (!gpsSupported) {
        	Constants.WIDGETS_DEFAULT.remove(Constants.WIDGET_LOCATION);
        }

        SharedPreferences preference = context.getSharedPreferences("appwidget_buttons", Context.MODE_PRIVATE);
        
        String trackers = preference.getString("trackers_array", null);
        if (trackers == null) {
            Log.i(TAG, "Default buttons being loaded");
            trackers = TextUtils.join(Constants.WIDGET_DELIMITER, Constants.WIDGETS_DEFAULT);
        }
        
        Log.i(TAG, "Buttons list: " + trackers);

        for (String tracker : trackers.split("\\|")) {
            StateTracker st = null;
            if (tracker.equals(Constants.WIDGET_WIFI)) {
            	st = new WifiStateTracker();
            } else if (tracker.equals(Constants.WIDGET_BLUETOOTH)) {
            	st = new BluetoothStateTracker();
            } else if (tracker.equals(Constants.WIDGET_LOCATION)) {
            	st = new LocationStateTracker();
            } else if (tracker.equals(Constants.WIDGET_SYNC)) {
            	st = new SyncStateTracker();
            } else if (tracker.equals(Constants.WIDGET_MOBILEDATA)) {
            	st = new MobileDataStateTracker();
            } else if (tracker.equals(Constants.WIDGET_BRIGHTNESS)) {
            	st = new BrightnessStateTracker();
            } else if (tracker.equals(Constants.WIDGET_FLASHLIGHT)) {
            	st = new FlashlightStateTracker();
            } else if (tracker.equals(Constants.WIDGET_AIRPLANEMODE)) {
            	st = new AirplaneModeStateTracker();
            }
            
            if (st != null) {
            	sStateTrackers.add(st);
            }

        }
        
/*        StringBuilder sb = new StringBuilder("sStateTrackers: ");
    	for (StateTracker st : sStateTrackers) {
    		sb.append(st.getName() + " | ");
    	}
    	Log.d(TAG, sb.toString());*/
    }
    
    private static void shutdown(Context context) {
        if (sObserver != null) {
            context.getContentResolver().unregisterContentObserver(sObserver);
        }
    	sStateTrackers.clear();
    }
    
    private static void setupButtons(Context context) {
        shutdown(context);
        sObserver = new SettingsObserver(mHandler, context);
        sObserverMap.clear();
        loadTrackers(context);
        setupContentObserver(context);
    }
    
    private static void setupContentObserver(Context context) {
        ContentResolver resolver = context.getContentResolver();
        for (Uri uri : sObserverMap.keySet()) {
            resolver.registerContentObserver(uri, false, sObserver);
        }
    }
    
    private static class SettingsObserver extends ContentObserver {
        private Context mContext;

        SettingsObserver(Handler handler, Context context) {
            super(handler);
            mContext = context;
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
        	Log.d(TAG, "SettingsObserver onChange uri : " + uri.toString());
	        if (sObserverMap != null && sObserverMap.get(uri) != null) {
	            for (StateTracker st : sObserverMap.get(uri)) {
	                st.onChangeUri(mContext, uri);
	            }
	        }
        }
    }
    
    private static void registerInMap(Object item, StateTracker tracker, HashMap map) {
        if (map.keySet().contains(item)) {
            ArrayList list = (ArrayList) map.get(item);
            if (!list.contains(tracker)) {
                list.add(tracker);
            }
        } else {
            ArrayList<StateTracker> list = new ArrayList<StateTracker>();
            list.add(tracker);
            map.put(item, list);
        }
    }

    public static void registerObservedContent(Uri uri, StateTracker tracker) {
        registerInMap(uri, tracker, sObserverMap);
    }
}
