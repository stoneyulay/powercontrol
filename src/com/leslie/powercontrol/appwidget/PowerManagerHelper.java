package com.leslie.powercontrol.appwidget;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.os.PowerManager;

public class PowerManagerHelper {

    private static Method sGetMinimumScreenBrightnessSettingMethod;

    private static Method sGetMaximumScreenBrightnessSettingMethod;
    
    private static Method sGetDefaultScreenBrightnessSettingMethod;;
    
    private static Method sSetBacklightBrightnessMethod;
    
    static {

    	try {
			sGetMinimumScreenBrightnessSettingMethod = PowerManager.class.getMethod("getMinimumScreenBrightnessSetting");
			sGetMaximumScreenBrightnessSettingMethod = PowerManager.class.getMethod("getMaximumScreenBrightnessSetting");
			sGetDefaultScreenBrightnessSettingMethod = PowerManager.class.getMethod("getDefaultScreenBrightnessSetting");
			sSetBacklightBrightnessMethod = PowerManager.class.getMethod("setBacklightBrightness", int.class);
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    /**
     * Gets the minimum supported screen brightness setting.
     * The screen may be allowed to become dimmer than this value but
     * this is the minimum value that can be set by the user.
     */
    public static int getMinimumScreenBrightnessSetting(PowerManager pm) {
    	Object brightness = null;
    	try {
			brightness = sGetMinimumScreenBrightnessSettingMethod.invoke(pm);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	if (brightness == null) {
    		return 0;
    	}
    	return ((Integer)brightness);
    }

    /**
     * Gets the maximum supported screen brightness setting.
     * The screen may be allowed to become dimmer than this value but
     * this is the maximum value that can be set by the user.
     */
    public static int getMaximumScreenBrightnessSetting(PowerManager pm) {
    	Object brightness = null;
    	try {
			brightness = sGetMaximumScreenBrightnessSettingMethod.invoke(pm);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	if (brightness == null) {
    		return 0;
    	}
    	return ((Integer)brightness);
    }

    /**
     * Gets the default screen brightness setting.
     */
    public static int getDefaultScreenBrightnessSetting(PowerManager pm) {
    	Object brightness = null;
    	try {
			brightness = sGetDefaultScreenBrightnessSettingMethod.invoke(pm);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	if (brightness == null) {
    		return 0;
    	}
    	return ((Integer)brightness);
    }

    /**
     * Sets the brightness of the backlights (screen, keyboard, button).
     * <p>
     * Requires the {@link android.Manifest.permission#DEVICE_POWER} permission.
     * </p>
     *
     * @param brightness The brightness value from 0 to 255.
     *
     */
    public static void setBacklightBrightness(PowerManager pm, int brightness) {
    	try {
			sSetBacklightBrightnessMethod.invoke(pm, brightness);
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    }
}
