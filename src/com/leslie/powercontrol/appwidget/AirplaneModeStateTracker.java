package com.leslie.powercontrol.appwidget;

import com.leslie.powercontrol.R;
import com.leslie.powercontrol.util.Constants;
import com.leslie.powercontrol.util.DebugLog;
import com.leslie.powercontrol.util.RootChecker;
import com.leslie.powercontrol.util.ShellUtils;
import com.leslie.powercontrol.util.Utils;
import com.leslie.powercontrol.util.ShellUtils.CommandResult;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.provider.Settings;

public class AirplaneModeStateTracker extends ToggleStateTracker {

	public AirplaneModeStateTracker() {
		super();
		PowerControlAppWidgetProvider.registerObservedContent(Settings.Global.getUriFor(Settings.Global.AIRPLANE_MODE_ON), this);
	}

	@Override
	public int getButtonDescription() {
		return R.string.gadget_airplane_mode;
	}

	@Override
	public int getButtonImageId(boolean on) {
		return R.drawable.icon_toggle_airplane_mode;
	}

	@Override
	public void onActualStateChange(Context context, Intent intent) {
		setCurrentState(context, getActualState(context));
	}

	@Override
	public int getActualState(Context context) {
        return (Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) == 1) ?
                		STATE_ENABLED : STATE_DISABLED;
	}

	@Override
	protected void requestStateChange(final Context context, final boolean desiredState) {
        final ContentResolver resolver = context.getContentResolver();
        final boolean currentState = (Settings.Global.getInt(resolver,
                Settings.Global.AIRPLANE_MODE_ON, 0) == 1);
        
        if (Utils.hasWriteSecurePermissions(context)) {
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... args) {
                	Integer newState = null;
                    // Turning Airplane mode on.
                    if (desiredState) {
                        if (!currentState) {
                        	newState = 1;
                        }
                    }

                    // Turning Airplane mode off.
                    if (currentState) {
                    	newState = 0;
                    }
                    
                    if (newState != null) {
                    	Settings.Global.putInt(resolver, Settings.Global.AIRPLANE_MODE_ON, newState.intValue());
                    	
                        // Post the intent
                        Intent intent = new Intent(Intent.ACTION_AIRPLANE_MODE_CHANGED);
                        intent.putExtra("state", desiredState);
                        context.sendBroadcast(intent);
                    }
                    
                    return getActualState(context) == STATE_ENABLED;
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    setCurrentState(
                        context,
                        result ? STATE_ENABLED : STATE_DISABLED);
                    PowerControlAppWidgetProvider.updateWidget(context);
                }
            }.execute();
        } else if (RootChecker.getInstance().hasRooted()) {
            new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... args) {
                	Integer newState = null;
                    // Turning Airplane mode on.
                    if (desiredState) {
                        if (!currentState) {
                        	newState = 1;
                        }
                    }

                    // Turning Airplane mode off.
                    if (currentState) {
                    	newState = 0;
                    }

                    if (newState != null) {
                    	// settings commands
                        StringBuilder settingsCommand = new StringBuilder().append("LD_LIBRARY_PATH=/vendor/lib:/system/lib settings put global ")
                        		.append(Settings.Global.AIRPLANE_MODE_ON + " ")
                        		.append(newState.toString());

                        DebugLog.d("settingsCommand : " + settingsCommand.toString());
                        CommandResult settingsCommandResult = ShellUtils.execCommand(settingsCommand.toString(), !Utils.isSystemApplication(context), true);
                        if (settingsCommandResult.successMsg != null
                                && (settingsCommandResult.successMsg.contains("Success") || settingsCommandResult.successMsg.contains("success"))) {
                            return true;
                        }

                        DebugLog.e(
                                new StringBuilder().append("wreite settings successMsg:").append(settingsCommandResult.successMsg)
                                        .append(", ErrorMsg:").append(settingsCommandResult.errorMsg).toString());

                        if (settingsCommandResult.result != 0) {
                        	startWirelessSettingsActivity(context);
                        }
                        
                        // am broadcast commands
                        StringBuilder amCommand = new StringBuilder().append("LD_LIBRARY_PATH=/vendor/lib:/system/lib am broadcast -a android.intent.action.AIRPLANE_MODE --ez state ")
                        		.append(desiredState);
                        
                        DebugLog.d("amCommand : " + amCommand.toString());
                        CommandResult amCommandResult = ShellUtils.execCommand(amCommand.toString(), !Utils.isSystemApplication(context), true);
                        if (amCommandResult.successMsg != null
                                && (amCommandResult.successMsg.contains("Success") || amCommandResult.successMsg.contains("success"))) {
                            return true;
                        }

                        DebugLog.e(
                                new StringBuilder().append("wreite settings successMsg:").append(amCommandResult.successMsg)
                                        .append(", ErrorMsg:").append(amCommandResult.errorMsg).toString());
                    }
                    return getActualState(context) == STATE_ENABLED;
                }

                @Override
                protected void onPostExecute(Boolean result) {
                    setCurrentState(
                        context,
                        result ? STATE_ENABLED : STATE_DISABLED);
                    PowerControlAppWidgetProvider.updateWidget(context);
                }
            }.execute();
        } else {
	        new AsyncTask<Void, Void, Boolean>() {
	            @Override
	            protected Boolean doInBackground(Void... args) {
	            	startWirelessSettingsActivity(context);
	
	                return getActualState(context) == STATE_ENABLED;
	            }
	
	            @Override
	            protected void onPostExecute(Boolean result) {
	                setCurrentState(
	                    context,
	                    result ? STATE_ENABLED : STATE_DISABLED);
	                PowerControlAppWidgetProvider.updateWidget(context);
	            }
	        }.execute();
        }

    }

	private void startWirelessSettingsActivity(Context context) {
        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
	}
	
	@Override
	public String getName() {
		return Constants.WIDGET_AIRPLANEMODE;
	}
	
}
