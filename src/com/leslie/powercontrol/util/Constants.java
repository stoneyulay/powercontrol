package com.leslie.powercontrol.util;

import java.util.ArrayList;

public class Constants {
    public static final String WIDGET_MOBILEDATA = "Mobile_data";
    public static final String WIDGET_SYNC = "Sync";
    public static final String WIDGET_WIFI = "Wifi";
    public static final String WIDGET_FLASHLIGHT = "Flashlight";
    public static final String WIDGET_LOCATION = "Location";
    public static final String WIDGET_BLUETOOTH = "Bluetooth";
    public static final String WIDGET_BRIGHTNESS = "Brightness";
    public static final String WIDGET_AIRPLANEMODE = "Airplane_mode";
    public static final String WIDGET_AUTOROTATE = "Auto_rotate";
    
    public static final String WIDGET_DELIMITER = "|";
    public static ArrayList<String> WIDGETS_DEFAULT = new ArrayList<String>();

    static {
    	WIDGETS_DEFAULT.add(WIDGET_MOBILEDATA);
    	WIDGETS_DEFAULT.add(WIDGET_SYNC);
    	WIDGETS_DEFAULT.add(WIDGET_WIFI);
    	WIDGETS_DEFAULT.add(WIDGET_FLASHLIGHT);
    	WIDGETS_DEFAULT.add(WIDGET_LOCATION);
    	WIDGETS_DEFAULT.add(WIDGET_BLUETOOTH);
    	WIDGETS_DEFAULT.add(WIDGET_BRIGHTNESS);
    	WIDGETS_DEFAULT.add(WIDGET_AIRPLANEMODE);
    }
}
