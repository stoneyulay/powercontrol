package com.leslie.powercontrol.util;

import java.io.File;

public class RootChecker {
    
    /**
     * The set of SU location I know by now.
     */
    public static final String[] SU_BINARY_DIRS = {
            "/system/bin", "/system/sbin", "/system/xbin",
            "/vendor/bin", "/sbin"
    };
    
    private static RootChecker instance;
    private Boolean hasRooted = null;
    
    private RootChecker() {
    	
    }
    
    public static synchronized RootChecker getInstance() {
        if (instance == null) {
            instance = new RootChecker();
        }
        return instance;
    }
    
    /**
     * Try to check if the device has been rooted.
     * <p>
     * Generally speaking, a device which has been rooted successfully must have
     * a binary file named SU. However, SU may not work well for those devices
     * rooted unfinished.
     * </p>
     * 
     * @return the result whether this device has been rooted.
     */
    public boolean hasRooted() {
        if (hasRooted == null) {
            for (String path : SU_BINARY_DIRS) {
                File su = new File(path + "/su");
                if (su.exists()) {
                    hasRooted = true;
                    break;
                } else {
                    hasRooted = false;
                }
            }
        }

        return hasRooted;
    }
}
