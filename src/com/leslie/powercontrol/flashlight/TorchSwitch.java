package com.leslie.powercontrol.flashlight;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.List;

public class TorchSwitch extends BroadcastReceiver {

    public static final String TOGGLE_FLASHLIGHT = "com.leslie.powercontrol.flashlight.TOGGLE_FLASHLIGHT";
    public static final String TORCH_STATE_CHANGED = "com.leslie.powercontrol.flashlight.TORCH_STATE_CHANGED";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(TOGGLE_FLASHLIGHT)) {
            // bright setting can come from intent or from prefs depending on
            // on what send the broadcast
            //
            // Unload intent extras if they exist:
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            boolean bright = intent.getBooleanExtra("bright", prefs.getBoolean("bright", false));

            Intent i = new Intent(context, TorchService.class);
            if (this.torchServiceRunning(context)) {
                context.stopService(i);
            } else {
                i.putExtra("bright", bright);
                context.startService(i);
            }
        }
    }

    private boolean torchServiceRunning(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Activity.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> svcList = am.getRunningServices(100);

        for (RunningServiceInfo serviceInfo : svcList) {
            ComponentName serviceName = serviceInfo.service;
            if (serviceName.getClassName().endsWith(".TorchService")
                    || serviceName.getClassName().endsWith(".RootTorchService"))
                return true;
        }
        return false;
    }
}

