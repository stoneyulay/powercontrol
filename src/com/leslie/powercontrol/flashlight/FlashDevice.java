package com.leslie.powercontrol.flashlight;

import java.io.IOException;

import com.leslie.powercontrol.R;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public class FlashDevice {
    private static final String MSG_TAG = "TorchDevice";
    
    /* New variables, init'ed by resource items */
    private static int mValueOff;
    private static int mValueOn;
    private static int mValueLow;
    private static int mValueHigh;
    private static int mValueDeathRay;
    private WakeLock mWakeLock;

    public static final int STROBE    = -1;
    public static final int OFF       = 0;
    public static final int ON        = 1;
    public static final int HIGH      = 128;
    public static final int DEATH_RAY = 3;
    
    private static FlashDevice sInstance;
    
    private int mFlashMode = OFF;

    private Camera mCamera = null;
    private SurfaceTexture mSurfaceTexture = null;

    private FlashDevice(Context context) {
        mValueOff = context.getResources().getInteger(R.integer.FlashDevice_valueOff);
        mValueOn = context.getResources().getInteger(R.integer.FlashDevice_valueOn);
        mValueLow = context.getResources().getInteger(R.integer.FlashDevice_valueLow);
        mValueHigh = context.getResources().getInteger(R.integer.FlashDevice_valueHigh);
        mValueDeathRay = context.getResources().getInteger(R.integer.FlashDevice_valueDeathRay);

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Torch");
    }
    
    public static synchronized FlashDevice instance(Context context) {
        if (sInstance == null) {
            sInstance = new FlashDevice(context.getApplicationContext());
        }
        return sInstance;
    }
    
    public synchronized void setFlashMode(int mode) {
    	Log.d(MSG_TAG, "setFlashMode " + mode);
        try {
            int value = mode;
            switch (mode) {
                case STROBE:
                    value = OFF;
                    break;
                case DEATH_RAY:
                    if (mValueDeathRay >= 0) {
                        value = mValueDeathRay;
                    } else if (mValueHigh >= 0) {
                        value = mValueHigh;
                    } else {
                        value = 0;
                        Log.d(MSG_TAG,"Broken device configuration");
                    }
                    break;
                case ON:
                    if (mValueOn >= 0) {
                        value = mValueOn;
                    } else {
                        value = 0;
                        Log.d(MSG_TAG,"Broken device configuration");
                    }
                    break;
            }

            if (mCamera == null) {
                mCamera = Camera.open();
            }
            if (value == OFF) {
                Camera.Parameters params = mCamera.getParameters();
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                mCamera.setParameters(params);
                if (mode != STROBE) {
                    mCamera.stopPreview();
                    mCamera.release();
                    mCamera = null;
                    if (mSurfaceTexture != null) {
                        mSurfaceTexture.release();
                        mSurfaceTexture = null;
                    }
                }
                if (mWakeLock.isHeld()) {
                    mWakeLock.release();
                }
            } else {
                if (mSurfaceTexture == null) {
                    // Create a dummy texture, otherwise setPreview won't work on some devices
                    mSurfaceTexture = new SurfaceTexture(0);
                    mCamera.setPreviewTexture(mSurfaceTexture);
                    mCamera.startPreview();
                }
                Camera.Parameters params = mCamera.getParameters();
                params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mCamera.setParameters(params);
                if (!mWakeLock.isHeld()) {
                    mWakeLock.acquire();
                }
            }


            mFlashMode = mode;
        } catch (IOException e) {
            if (mWakeLock.isHeld()) {
                mWakeLock.release();
            }
            throw new RuntimeException("Can't open flash device", e);
        }
    }

    public synchronized int getFlashMode() {
        return mFlashMode;
    }
}
