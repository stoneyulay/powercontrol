package com.leslie.powercontrol.flashlight;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

public class TorchService extends Service {
    private static final String MSG_TAG = "TorchRoot";

    private int mFlashMode;

    private static final int MSG_UPDATE_FLASH = 1;

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            final FlashDevice flash = FlashDevice.instance(TorchService.this);

            switch (msg.what) {
                case MSG_UPDATE_FLASH:
                    flash.setFlashMode(mFlashMode);
                    removeMessages(MSG_UPDATE_FLASH);
                    sendEmptyMessageDelayed(MSG_UPDATE_FLASH, 100);
                    break;
            }
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(MSG_TAG, "Starting torch");

        if (intent == null) {
            stopSelf();
            return START_NOT_STICKY;
        }

        mFlashMode = intent.getBooleanExtra("bright", false)
                ? FlashDevice.DEATH_RAY : FlashDevice.ON;

        mHandler.sendEmptyMessage(MSG_UPDATE_FLASH);

        updateState(true);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        mHandler.removeCallbacksAndMessages(null);
        FlashDevice.instance(this).setFlashMode(FlashDevice.OFF);
        updateState(false);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void updateState(boolean on) {
        Intent intent = new Intent(TorchSwitch.TORCH_STATE_CHANGED);
        intent.putExtra("state", on ? 1 : 0);
        sendStickyBroadcast(intent);
    }
}
